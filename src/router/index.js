import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import Plan from '@/views/AccessControl/Plan'
import DoorGroup from '@/views/AccessControl/DoorGroup'
import Time from '@/views/consume/Time'
import BussinessEntity from '@/views/consume/BussinessEntity'
import CardPrivilege from '@/views/AccessControl/CardPrivilege'
import AdministerRecharge from '@/views/consume/AdministerRecharge'
import UserRecharge from '@/views/consume/UserRecharge'
import AdministerRefund from '@/views/consume/AdministerRefund'
import UserCardConsumptionRecord from '@/views/consume/UserCardConsumptionRecord'
import UserCardCashRecord from '@/views/consume/UserCardCashRecord'
import AdministerCardConsumptionRecord from '@/views/consume/AdministerCardConsumptionRecord'
import AdministerCardCashRecord from '@/views/consume/AdministerCardCashRecord'
import MerchantStatisticalReport from '@/views/consume/MerchantStatisticalReport'
import AdministerConsumptionReport from '@/views/consume/AdministerConsumptionReport'
import AdministerCashReport from '@/views/consume/AdministerCashReport'
import AdministerBalanceReport from '@/views/consume/AdministerBalanceReport'
import Devices from '@/views/consume/Devices'
import CardGroup from '@/views/vims/CardGroup'
import FaceAuth from '@/views/vims/FaceAuth'
import Card from '@/views/card/card'

import ChannelControl from '@/views/AccessControl/ChannelControl'


/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/access',
    component: Layout,
    name: '门禁管理',
    redirect: '/access/plan',
    meta: { title: '门禁管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'plan',
        name: '开门计划',
        component: Plan,
        meta: { title: '开门计划', icon: 'dashboard' }
      },
      {
        path: 'group',
        name: '门组',
        component: DoorGroup,
        meta: { title: '门组', icon: 'dashboard' }
      },

      {
        path: 'privilege',
        name: '人脸授权',
        component: CardPrivilege,
        meta: { title: '人脸授权', icon: 'dashboard' }
      },
      {
        path: 'channelControl',
        name: '门禁控制',
        component: ChannelControl,
        meta: { title: '门禁控制', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/consume',
    component: Layout,
    name: '消费模块',
    redirect: '/consume/BussinessEntity',
    meta: { title: '消费模块', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'bussinessEntity',
        name: '营业单位',
        component: BussinessEntity,
        meta: { title: '营业单位', icon: 'dashboard' }
      },
      {
        path: 'time',
        name: '营业时段',
        component: Time,
        meta: { title: '营业时段', icon: 'dashboard' }
      },
      {
        path: 'devices',
        name: '消费设备管理',
        component: Devices,
        meta: { title: '消费设备管理', icon: 'dashboard' }
      },
      {
        path: 'administer_recharge',
        name: '充值-管理者',
        component: AdministerRecharge,
        meta: { title: '充值-管理者', icon: 'dashboard' }
      },
      {
        path: 'user_recharge',
        name: '余额/充值-消费者',
        component: UserRecharge,
        meta: { title: '余额/充值-消费者', icon: 'dashboard' }
      },
      {
        path: 'administer_refund',
        name: '退款-管理者',
        component: AdministerRefund,
        meta: { title: '退款-管理者', icon: 'dashboard' }
      },
      {
        path: 'user_card_consumption_record',
        name: '消费记录查询-消费者',
        component: UserCardConsumptionRecord,
        meta: { title: '消费记录查询-消费者', icon: 'dashboard' }
      },
      {
        path: 'user_card_cash_record',
        name: '充值记录查询-消费者',
        component: UserCardCashRecord,
        meta: { title: '充值记录查询-消费者', icon: 'dashboard' }
      },
      {
        path: 'administer_card_consumption_record',
        name: '消费记录查询-管理者',
        component: AdministerCardConsumptionRecord,
        meta: { title: '消费记录查询-管理者', icon: 'dashboard' }
      },
      {
        path: 'administer_card_cash_record',
        name: '充值记录查询-管理者',
        component: AdministerCardCashRecord,
        meta: { title: '充值记录查询-管理者', icon: 'dashboard' }
      },
      {
        path: 'merchant_statistical_report',
        name: '营业收入统计-商家',
        component: MerchantStatisticalReport,
        meta: { title: '营业收入统计-商家', icon: 'dashboard' }
      },
      {
        path: 'administer_consumption_report',
        name: '人员消费统计-管理者',
        component: AdministerConsumptionReport,
        meta: { title: '人员消费统计-管理者', icon: 'dashboard' }
      },
      {
        path: 'administer_cash_report',
        name: '现金交易统计-管理者',
        component: AdministerCashReport,
        meta: { title: '现金交易统计-管理者', icon: 'dashboard' }
      },
      {
        path: 'administer_balance_report',
        name: '卡内余额统计-管理者',
        component: AdministerBalanceReport,
        meta: { title: '卡内余额统计-管理者', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/card',
    component: Layout,
    name: '卡管理',
    redirect: '/card/card',
    meta: { title: '卡管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'card',
        name: '卡列表',
        component: Card,
        meta: { title: '卡管理', icon: 'dashboard' }
      },
    ]
  },
  {
    path: '/vims',
    component: Layout,
    name: '可视对讲管理',
    redirect: '/vims/cardgroup',
    meta: { title: '可视对讲管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'cardgroup',
        name: '可视对讲分组',
        component: CardGroup,
        meta: { title: '可视对讲分组', icon: 'dashboard' }
      },
      {
        path: 'time',
        name: '人脸授权',
        component: FaceAuth,
        meta: { title: '人脸授权', icon: 'dashboard' }
      },
    ]
  },

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'el-icon-s-help' },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table' }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'Form', icon: 'form' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index'),
  //       name: 'Menu2',
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },
  //
  // {
  //   path: 'external-link',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
  //       meta: { title: 'External Link', icon: 'link' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
