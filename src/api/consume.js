import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/vue-admin-template/consume/list',
    method: 'get',
    params
  })
}

export function createEntity(data) {
  return request({
    url: '/vue-element-template/consume/create',
    method: 'post',
    data
  })
}

export function deleteEntity(data) {
  return request({
    url: '/vue-element-template/consume/delete',
    method: 'delete',
    data
  })
}
export function getTimeList(params) {
  return request({
    url: '/vue-admin-template/consume/timeList',
    method: 'get',
    params
  })
}
