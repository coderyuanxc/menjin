import request from '@/utils/request'
import da from "element-ui/src/locale/lang/da";

// 获取 通道信息
export function getChannel(param) {
  return request({
    url: '/admin/rest/api',
    method: 'post',
    data: param
  })
}

// 获取人脸授权任务

export function getAcsFaceSyncList(data) {
  return request({
    url: '/CardSolution/card/accessControl/doorAuthority/getAcsFaceSyncList',
    method: 'post',
    data
  })
}

// 获取卡片授权
export function getAcsCardList(data) {
  return request({
    url: '/CardSolution/card/accessControl/doorAuthority/bycondition/combined',
    method: 'post',
    data
  })
}


