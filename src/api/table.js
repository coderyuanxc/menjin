import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/vue-admin-template/table/list',
    method: 'get',
    params
  })
}

export function createArticle(data) {
  return request({
    url: '/vue-element-template/table/create',
    method: 'post',
    data
  })
}
