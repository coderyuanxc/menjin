import request from '@/utils/request'

export function getAllCardGroup(params) {
  return request({
    url: '/vims/cardGroup/list',
    method: 'get',
    params
  })
}