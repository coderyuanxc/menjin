import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/vue-admin-template/table/consumption',
    method: 'get',
    params: query
  })
}
