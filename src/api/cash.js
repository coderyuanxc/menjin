import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/vue-admin-template/table/cash',
    method: 'get',
    params: query
  })
}
