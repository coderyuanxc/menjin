import request from '@/utils/request'

export function getList(query) {
  return request({
    url: '/vue-admin-template/table/balance',
    method: 'get',
    params: query
  })
}
