import request from '@/utils/request'

export function getAllFaceAuth(pageNum, pageSize, owner) {
  return request({
    // url: '/vims/face/page/accredit?pageNum=' + pageNum +'&pageSize=' + pageSize + '&owner=' + owner + '&userId=xx&userName=xx&token=xx',
    url: '/vims/face/page/accredit?pageNum=' + pageNum +'&pageSize=' + pageSize + '&owner=' + owner,
    method: 'get',
  })
}

// 查询所有人员集合
export function getAllPeople(data) {
  return request({
    // url: '/CardSolution/card/person/bycondition/combined?userId=xx&userName=xx&token=xx',
    url: '/CardSolution/card/person/bycondition/combined',
    method: 'post',
    data: data
  })
}

// 查询所有的可视对讲分组
export function getCardGroup(data) {
  return request({
    // url: '/vims/cardGroup/list?userId=xx&userName=xx&token=xx',
    url: '/vims/cardGroup/list',
    method: 'post',
    data: data
  })
}

export function addFaceAuth(addData) {
  return request({
    // url: '/vims/face/auth/api?userId=xx&userName=xx&token=xx',
    url: '/vims/face/auth/api',
    method: 'post',
    data: addData
  })
}

export function getFaceAuthTasks(addData) {
  return request({
    // url: '/vims/faceTask/list?userId=xx&userName=xx&token=xx',
    url: '/vims/faceTask/list',
    method: 'get',
    data: addData
  })
}

export function addFaceAuthTask() {
  return request({
    url: '/vims/faceTask/list/add',
    method: 'post',
  })
}
