import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/vue-admin-template/card/list',
    method: 'get',
    params
  })
}

export function createEntity(data) {
  return request({
    url: '/vue-element-template/card/create',
    method: 'post',
    data
  })
}

export function getTimeList(params) {
  return request({
    url: '/vue-admin-template/card/timeList',
    method: 'get',
    params
  })
}
