import request from '@/utils/request'

export function getSwingCardSolution(query) {
    return request({
        url: '/CardSolution/card/accessControl/swingCardRecord',
        method: 'post',
        params: query
    })
}
export function getSwingCardSolutionBycondition(query) {
    return request({
        url: '/accessControl/swingCardRecord/bycondition/combined',
        method: 'post',
        query
    })
}

export function getCardSolutionIntegrate(query) {
    return request({
        url: '/integrate/bycondition/combined',
        method: 'post',
        query
    })
}