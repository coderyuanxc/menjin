import request from '@/utils/request'

export function getAllChannels(query) {
  return request({
    // url: '/CardSolution/card/accessControl/doorGroup/bycondition/combined?userId=xx&userName=xx&token=xx',
    url: '/admin/rest/api',
    method: 'post',
    data: query
  })
}

export function openDoor(channelCodeList) {
  return request({
    // url: '/CardSolution/card/accessControl/channelControl/openDoor?userId=xx&userName=xx&token=xx',
    url: '/CardSolution/card/accessControl/channelControl/openDoor',
    method: 'post',
    data: channelCodeList
  })
}
export function closeDoor(channelCodeList) {
  return request({
    // url: '/CardSolution/card/accessControl/channelControl/closeDoor?userId=xx&userName=xx&token=xx',
    url: '/CardSolution/card/accessControl/channelControl/closeDoor',
    method: 'post',
    data: channelCodeList
  })
}
export function stayOpen(channelCodeList) {
  return request({
    // url: '/CardSolution/card/accessControl/channelControl/stayOpen?userId=xx&userName=xx&token=xx',
    url: '/CardSolution/card/accessControl/channelControl/stayOpen',
    method: 'post',
    data: channelCodeList
  })
}
export function stayClose(channelCodeList) {
  return request({
    // url: '/CardSolution/card/accessControl/channelControl/stayClose?userId=xx&userName=xx&token=xx',
    url: '/CardSolution/card/accessControl/channelControl/stayClose',
    method: 'post',
    data: channelCodeList
  })
}
export function setNormal(channelCodeList) {
  return request({
    // url: '/CardSolution/card/accessControl/channelControl/normal?userId=xx&userName=xx&token=xx',
    url: '/CardSolution/card/accessControl/channelControl/normal',
    method: 'post',
    data: channelCodeList
  })
}
