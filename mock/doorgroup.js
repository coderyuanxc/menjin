const Mock = require('mockjs')

// const data = Mock.mock({
//   'items|10': [{
//     id: '@increment',
//     name:'@title(5, 10)',
//     memo:'@csentence(8, 12)',
//     'hasChildChannel|1': [true, false]
//   }]
// })

// const doorGroupAuthority = Mock.mock({
//   doorData: {
//     id: '',
//     name: '',
//     memo: '',
//     hasChildChannel: true,
//     'doorGroupDetail|5': [{
//       channelCode: '@increment',
//       channelName: '@ctitle(3, 6)',
//       'deviceCode|1-120': 20,
//       deviceName: '@ctitle(2, 6)',
//       deviceStatus: 'ON',
//       'groupId|+1': 10,
//       'id|+1': 1,
//       online: 1,
//       orgName:'根节点'
//     }]
//   },
// })

const data = [
  {
    id: 1,
    name: '1号门组',
    memo: '这是描述文字',
    hasChildChannel: true,
    doorGroupDetail: [
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 1,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 1,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 1,
      }
    ],
  },
  {
    id: 2,
    name: '2号门组',
    memo: '这是描述文字',
    hasChildChannel: true,
    doorGroupDetail: [
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 2,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 2,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 2,
      }
    ],
  },
  {
    id: 3,
    name: '3号门组',
    memo: '这是描述文字',
    hasChildChannel: true,
    doorGroupDetail: [
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 3,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 3,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 3,
      }
    ],
  },
  {
    id: 4,
    name: '4号门组',
    memo: '这是描述文字',
    hasChildChannel: false,
    doorGroupDetail: [
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 4,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 4,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 4,
      }
    ],
  },
  {
    id: 5,
    name: '5号门组',
    memo: '这是描述文字',
    hasChildChannel: false,
    doorGroupDetail: [
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 5,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 5,
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
        deviceStatus: "ON",
        id: 5,
      }
    ],
  },
  {
    id: 6,
    name: '6号门组',
    memo: '这是描述文字',
    hasChildChannel: true,
    doorGroupDetail: [
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
      },
      {
        channelCode: "1000001$7$0$0",
        channelName: "132_通道1",
        deviceCode: "1000001",
        deviceName: "132",
      }
    ],
  }
]

module.exports = [
  {
    url: '/CardSolution/card/accessControl/doorGroup/bycondition/combined',
    type: 'post',
    response: config => {
      // const items = data.items
      console.log(config)
      let name = config.body.name
      let items = []
      if(config.body.name != ''){
        for(let i = 0; i < data.length; ++i){
          if(data[i].name == name){
            items.push(data[i])
            break
          }
        }
      } else {
        items = data
      }
      return {
        code: 20000,
        data: {
          totalRows: items.length,
          pageData: items
        }
      }
    }
  },
  {
    url: '/CardSolution/card/accessControl/doorGroup/add',
    type: 'post',
    response: config => {
      let temp = config.body
      temp.id = data.length + 1;
      data.unshift(temp)
      return {
        code: 20000
      }
    }
  },
  {
    url: '/CardSolution/card/accessControl/doorGroup/id/*',
    type: 'post',
    response: config => {
      let temp = config.body
      let authority = []
      console.log(temp)
      for(let j = 0; j < data.length; ++j){
        if(data[j].id == temp.id){
          authority = data[j].doorGroupDetail
        }
      }
      return {
        code: 20000,
        data: {
          doorGroupDetail: authority
        }
      }
    }
  },
  {
    url: '/CardSolution/card/accessControl/doorGroup/update',
    type: 'post',
    response: config => {
      let temp = config.body
      for(let i = 0; i < data.length; ++i){
        if(data[i].id == temp.id){
          data[i] = temp;
          break;
        }
      }
      return {
        code: 20000,
      }
    }
  },
  {
    url: '/CardSolution/card/accessControl/doorGroup/delete/batch',
    type: 'post',
    response: config => {
      let temp = config.body
      console.log(temp)
      for(let i = 0; i < temp.length; ++i){
        for(let j = 0; j < data.length; ++j){
          if(data[j].id == temp[i]){
            data.splice(j,1)
          }
        }
      }
      return {
        code: 20000,
      }
    }
  }
]

// Mock.mock("/CardSolution/card/accessControl/doorGroup/bycondition/combined", "post", (options) =>{
//   // 最佳实践，将请求和参数都打印出来，以便调试A
//   let item = JSON.parse(options.body)
//   console.log(item)
//   let data = [];
//   for (let i = 0; i < 10; i++) {
//     let newObject = {
//       // title: Random.csentence(5, 30), // Random.csentence( min, max )
//       // thumbnail_pic_s: Random.dataImage('300x250', 'mock的图片'), // Random.dataImage( size, text ) 生成一段随机的 Base64 图片编码
//       // author_name: Random.cname(), // Random.cname() 随机生成一个常见的中文姓名
//       // date: Random.date() + ' ' + Random.time() // Random.date()指示生成的日期字符串的格式,默认为yyyy-MM-dd；Random.time() 返回一个随机的时间字符串
//       id: '@increment',
//       name:'@title(5, 10)',
//       memo:'@title(5, 10)',
//       'hasChildChannel|1': [true, false]
//     }
//     data.push(newObject)
//   }
//   const items = data
//   return {
//     code: 20000,
//     data: {
//       totalRows: items.length,
//       pageData: items
//     }
//   }
// });
