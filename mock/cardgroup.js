const Mock = require('mockjs')

const data = Mock.mock({
  'items|10': [{
    groupName: "@cname()" + "的分组计划",
    memo: "@csentence()",
    "id|1-100": 1,
    groupType: 1
  }]
})
module.exports = [{
  url: '/vims/cardGroup/list',
  type: 'get',
  response: config => {
    const items = data.items;
    return {
      code: 20000,
      message: 'success',
      data: {
        items: items
      }
    }
  }
}]