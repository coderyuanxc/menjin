const Mock = require('mockjs')

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(
    Mock.mock({
      personId: '@string("number", 6)',
      personCode: '@string("number", 6)',
      personName: '@cname',
      deptId: '@string("number", 3)',
      deptName: '@word',
      cardNumber: '@string("number", 10)',
      balance: '@float(0, 1000, 0, 2)'
    })
  )
}

// const data = Mock.mock({
//   'items|200': [{
//     personId: '@string("number", 6)',
//     personCode: '@string("number", 6)',
//     personName: '@cname',
//     deptId: '@string("number", 3)',
//     deptName: '@word',
//     cardNumber: '@string("number", 10)',
//     balance: '@float(0, 1000, 0, 2)'
//   }]
// })

module.exports = [
  {
    url: '/vue-admin-template/table/balance',
    type: 'get',
    response: config => {
      // console.log('执行到这')
      const { pageNum, pageSize, deptIdsString, personName, personCode, queryTime } = config.query

      const mockList = List.filter(item => {
        // if (importance && item.importance !== +importance) return false
        // if (type && item.type !== type) return false
        if (deptIdsString && item.deptId !== deptIdsString) return false
        if (personName && item.personName.indexOf(personName) < 0) return false
        if (personCode && item.personCode !== personCode) return false
        console.log(queryTime)
        return true
      })

      const pageData = mockList.filter((item, index) => index < pageSize * pageNum && index >= pageSize * (pageNum - 1))
      // console.log('执行到这')
      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageData
        }
      }
    }
  }
]
