const Mock = require('mockjs')

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(
    Mock.mock({
      merchantId: '@string("number", 2)',
      terminalId: '@string("number", 6)',
      terminalName: '@word',
      breakfastAmount: '@integer(0, 3000)',
      lunchAmount: '@integer(0, 3000)',
      dinnerAmount: '@integer(0, 3000)',
      supperAmount: '@integer(0, 3000)',
      consumptionTimes: '@integer(0, 500)',
      totalAmount: function() {
        return this.breakfastAmount + this.lunchAmount + this.dinnerAmount + this.supperAmount
      },
      reportDate: '@date("yyyy-MM-dd")'
    })
  )
}

function dateCompare(date1,date2){
  date1 = date1.replace(/\-/gi,"/");
  date2 = date2.replace(/\-/gi,"/");
  var time1 = new Date(date1).getTime();
  var time2 = new Date(date2).getTime();
  console.log(time1, time2)
  if(time1 > time2){
    return false;
  }else{
    return true;
  }
}

module.exports = [
  {
    url: '/vue-admin-template/table/income',
    type: 'get',
    response: config => {
      const { pageNum, pageSize, startDate, endDate } = config.query
      // console.log(startDate)

      const mockList = List.filter(item => {
        if (startDate && dateCompare(item.reportDate, startDate)) return false
        if (startDate && dateCompare(endDate, item.reportDate)) return false
        // console.log(startDate, item.reportDate, endDate)  // 打印2003-1-1 2006-10-14 2017-2-24
        return true
      })

      const pageData = mockList.filter((item, index) => index < pageSize * pageNum && index >= pageSize * (pageNum - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageData
        }
      }
    }
  }
]
