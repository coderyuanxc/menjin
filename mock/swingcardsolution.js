const Mock = require('mockjs')
let data = []
let deptNames = ["保卫处", "人事部", "销售部"]
count = 10
for (let i = 0; i < count; i++) {
    data.push(Mock.mock({
        id: '@id',
        deptIds: '@integer(0,2)',//生成部门的id作为搜索的根据 返回数据但是不显示在table
        cardNumber: '@natural',
        'cardStatus|0-3': 1,
        'cardType|0-2': 1,
        channelCode: '@string("number", 12)',
        channelName: "@cword(3.5)",
        deptName: function () {
            let dept = this.deptIds;
            return deptNames[dept]
        },
        deviceCode: '@string("number", 10)',
        deviceName: "@cword(3.5)",
        'enterOrExit|1-2': 1,
        'openResult|0-1': 1,
        openType: '@string("number",4)',
        paperNumber: '@string("number", 11)',
        personCode: '@string("number", 10)',
        personId: '@string("number", 10)',
        personName: '@cname',
        swingTime: '@datetime("yyyy-MM-dd HH:mm:ss")',
        recordImage: '@image("200*100")',
        errorDetail: "@cword(3.5)",

    })
    )
}
let integrateData = []
for (let i = 0; i < count; i++) {
    integrateData.push(Mock.mock({
        name: '@cname',
        'age|18-55': 1,
        'compareResult|1': ['比对成功', '比对失败'],
        'address|1': ['浙江', '山东', '河南', '北京'],
        createTime: '@datetime("yyyy-MM-dd HH-mm-ss")',
        'id|1-100': 1,
        paperNumber: '@string("number", 11)',
        'sex|1': ['男', '女'],
        deviceName: "@cword(3.5)",
        facePhoto: '@image("200x100")',
        paperNumberImage: '@image("200x100")'
    }))
}
// let a = "1"
// console.log(data)
// const selectedList = data.filter(item => {
//     if (item.deptIds != parseInt(a)) return false;
//     return true;
// });
// console.log(selectedList);

module.exports = [
    {
        url: '/CardSolution/card/accessControl/swingCardRecord',
        type: 'post',
        response: config => {
            const items = data
            return {
                code: 20000,
                message: 'success',
                data: {
                    items: items
                }
            }
        }
    },
    {
        url: '/accessControl/swingCardRecord/bycondition/combined',
        type: 'post',
        response: config => {
            const { deptIds } = config.query
            console.log(deptIds)
            const Mockitems = data.filter(item => {
                if (item.deptIds != parseInt(deptIds)) return false;
                return true;
            })
            return {
                code: 20000,
                message: 'success',
                data: {
                    deptIds: deptIds,
                    items: Mockitems
                }
            }
        }
    },
    {
        url: '/integrate/bycondition/combined',
        type: 'post',
        response: config => {
            const items = integrateData
            return {
                code: 20000,
                message: 'success',
                data: {
                    items: items
                }
            }
        }
    }
]