const Mock = require('mockjs')

const channels = [
  {
    "category": 21,
    "channelId": "1000000$1$0$0",
    "channelName": "园区1幢 1单元数字门口机(白光)_01_1",
    "deviceId": 1,
    "deviceIp": "10.35.120.185",
    "deviceName": "园区1幢 1单元数字门口机(白光)_01",
    "isDeviceOnline": true,
    "orgCode": "001",
    "type": "11",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 21,
    "channelId": "1000005$1$0$0",
    "channelName": "IPC",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60001",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 21,
    "channelId": "1000005$1$0$1",
    "channelName": "camera1",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60002",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 21,
    "channelId": "1000005$1$0$2",
    "channelName": "51ipc",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60003",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 8,
    "channelId": "1000005$1$0$3",
    "channelName": "IP PTZ Camera",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60004",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 8,
    "channelId": "1000005$1$0$4",
    "channelName": "IPC127",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60005",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 8,
    "channelId": "1000005$1$0$5",
    "channelName": "20.2.22.32_6",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60006",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 8,
    "channelId": "1000005$1$0$6",
    "channelName": "20.2.22.32_7",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60007",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 8,
    "channelId": "1000005$1$0$7",
    "channelName": "20.2.22.32_8",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60008",
    "type": "15",
    "unitType": 7,
    "status": 2,
  },
  {
    "category": 8,
    "channelId": "1000005$1$0$8",
    "channelName": "20.2.22.32_9",
    "deviceId": 6,
    "deviceIp": "20.2.22.32",
    "deviceName": "20.2.22.32",
    "isDeviceOnline": false,
    "orgCode": "001001",
    "sn": "60009",
    "type": "15",
    "unitType": 7,
    "status": 2,
  }

]
const acsFaceData = [
  {
    "channelCode": "1000019$7$0$0",
    "channelName": "TEST_1",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 1,
    "personCode": "90467",
    "personName": "lhc",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$1",
    "channelName": " TEST_2",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 2,
    "personCode": "999",
    "personName": "navida",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$2",
    "channelName": " TEST_3",
    "createTime": "2019-06-12 18:21:43",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 3,
    "personCode": "25",
    "personName": "lwn",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000015$7$0$3",
    "channelName": "TEST_4",
    "createTime": "2019-06-12 15:14:46",
    "deptName": "根节点",
    "deviceCode": "1000015",
    "operateType": 1,
    "personCode": "54433321",
    "personName": "AA",
    "syncFlag": "9",
    "updateTime": "2019-06-12 20:10:00"
  },
  {
    "channelCode": "1000019$7$0$5",
    "channelName": "TEST_5",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 1,
    "personCode": "90467",
    "personName": "lhc",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$6",
    "channelName": " TEST_6",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 2,
    "personCode": "999",
    "personName": "navida",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$7",
    "channelName": " TEST_7",
    "createTime": "2019-06-12 18:21:43",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 3,
    "personCode": "25",
    "personName": "lwn",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000015$7$0$8",
    "channelName": "TEST_8",
    "createTime": "2019-06-12 15:14:46",
    "deptName": "根节点",
    "deviceCode": "1000015",
    "operateType": 1,
    "personCode": "54433321",
    "personName": "AA",
    "syncFlag": "9",
    "updateTime": "2019-06-12 20:10:00"
  }, {
    "channelCode": "1000019$7$0$9",
    "channelName": "TEST_9",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 1,
    "personCode": "90467",
    "personName": "lhc",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$10",
    "channelName": " TEST_10",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 2,
    "personCode": "999",
    "personName": "navida",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$11",
    "channelName": " TEST_11",
    "createTime": "2019-06-12 18:21:43",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 3,
    "personCode": "25",
    "personName": "lwn",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000015$7$0$12",
    "channelName": "TEST_12",
    "createTime": "2019-06-12 15:14:46",
    "deptName": "根节点",
    "deviceCode": "1000015",
    "operateType": 1,
    "personCode": "54433321",
    "personName": "AA",
    "syncFlag": "9",
    "updateTime": "2019-06-12 20:10:00"
  },
  {
    "channelCode": "1000019$7$0$13",
    "channelName": "TEST_13",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 1,
    "personCode": "90467",
    "personName": "lhc",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$14",
    "channelName": " TEST_14",
    "createTime": "2019-06-12 18:21:44",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 2,
    "personCode": "999",
    "personName": "navida",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000019$7$0$15",
    "channelName": " TEST_15",
    "createTime": "2019-06-12 18:21:43",
    "deptName": "根节点",
    "deviceCode": "1000019",
    "operateType": 3,
    "personCode": "25",
    "personName": "lwn",
    "syncFlag": "3",
    "updateTime": "2019-06-12 18:34:01"
  },
  {
    "channelCode": "1000015$7$0$16",
    "channelName": "TEST_16",
    "createTime": "2019-06-12 15:14:46",
    "deptName": "根节点",
    "deviceCode": "1000015",
    "operateType": 1,
    "personCode": "54433321",
    "personName": "AA",
    "syncFlag": "9",
    "updateTime": "2019-06-12 20:10:00"
  }
]
const acsCardData = [
  {
    "authorizeStatus": "2",
    "cardNumber": "54454545",
    "cardPrivilegeDetailCount": 2,
    "cardStatus": "ACTIVE",
    "cardSyncCount": 1,
    "cardType": "普通卡",
    "deptId": 1,
    "deptName": "根节点",
    "id": 13,
    "personCode": "33456",
    "personName": "AAAA",
    "successCount": 1,
    "taskStatus": 1,
    "timeQuantumId": 1
  },
  {
    "authorizeStatus": "1",
    "cardNumber": "00000019",
    "cardPrivilegeDetailCount": 0,
    "cardStatus": "ACTIVE",
    "cardSyncCount": 0,
    "cardType": "普通卡",
    "deptId": 41,
    "deptName": "导入导出测试部门",
    "id": 6,
    "personCode": "11002",
    "personName": "导入2",
    "successCount": 0,
    "taskStatus": 0
  },
  {
    "authorizeStatus": "1",
    "cardNumber": "FAA11212",
    "cardPrivilegeDetailCount": 0,
    "cardStatus": "ACTIVE",
    "cardSyncCount": 0,
    "cardType": "普通卡",
    "deptId": 4,
    "deptName": "1幢",
    "id": 12,
    "personCode": "12312",
    "personName": "1访客",
    "successCount": 0,
    "taskStatus": 0
  }
]

module.exports = [
  {
    url: '/admin/rest/api',
    type: 'post',
    response: config => {
      const {param, pagination} = config.body
      let response = {
        code: 20000,
        data: channels,
        result: {
          code: "success"
        }
      }
      if (pagination !== undefined) {
        response.pagination = {
          currentPage: pagination.currentPage,
          pageSize: pagination.pageSize,
          pagesCount: 18,
          recordsCount: 179
        }
      }
      if (param !== undefined) {
        response.param = param
      }
      return response;
    }
  },
  {
    url: '/CardSolution/card/accessControl/channelControl/openDoor',
    type: 'post',
    response: config => {
      const channelCodeList = config.body
      channelCodeList.forEach( code => {
        channels.forEach(channel => {
          if(channel.channelId == code) {
            channel.status = 1
            console.log(channel.status)
          }
        })
      })
      let response = {
        code: 20000,
        data: channels,
        result: {
          code: "success"
        }
      }
      return response;
    }
  },
  {
    url: '/CardSolution/card/accessControl/channelControl/closeDoor',
    type: 'post',
    response: config => {
      const channelCodeList = config.body
      channelCodeList.forEach( code => {
        channels.forEach(channel => {
          if(channel.channelId == code) {
            channel.status = 2
            console.log(channel.status)
          }
        })
      })
      let response = {
        code: 20000,
        data: channels,
        result: {
          code: "success"
        }
      }
      return response;
    }
  },
  {
    url: '/CardSolution/card/accessControl/channelControl/stayOpen',
    type: 'post',
    response: config => {
      const channelCodeList = config.body
      channelCodeList.forEach( code => {
        channels.forEach(channel => {
          if(channel.channelId == code) {
            channel.status = 3
            console.log(channel.status)
          }
        })
      })
      let response = {
        code: 20000,
        data: channels,
        result: {
          code: "success"
        }
      }
      return response;
    }
  },
  {
    url: '/CardSolution/card/accessControl/channelControl/stayClose',
    type: 'post',
    response: config => {
      const channelCodeList = config.body
      channelCodeList.forEach( code => {
        channels.forEach(channel => {
          if(channel.channelId == code) {
            channel.status = 4
            console.log(channel.status)
          }
        })
      })
      let response = {
        code: 20000,
        data: channels,
        result: {
          code: "success"
        }
      }
      return response;
    }
  },
  {
    url: '/CardSolution/card/accessControl/channelControl/normal',
    type: 'post',
    response: config => {
      const channelCodeList = config.body
      channelCodeList.forEach( code => {
        channels.forEach(channel => {
          if(channel.channelId == code) {
            channel.status = 5
            console.log(channel.status)
          }
        })
      })
      let response = {
        code: 20000,
        data: channels,
        result: {
          code: "success"
        }
      }
      return response;
    }
  },
  {
    url: '/CardSolution/card/accessControl/doorAuthority/getAcsFaceSyncList',
    type: 'post',
    response: config => {
      const {pageNum, pageSize, param} = config.body;
      let responseData = {
        currentPage: pageNum,
        pageSize: pageSize,
        param: param
      }
      responseData.pageData = acsFaceData;
      // 条件查询
      if (param.personName !== '') {
        responseData.pageData = responseData.pageData.filter(item => {
          return item.personName.indexOf(param.personName) !== -1;
        })
      }
      if (param.deptIds !== '') {
        let ids = param.deptIds.split(",");
        responseData.pageData = responseData.pageData.filter(item => {
          let flag = false;
          ids.filter(id => {
            if (item.deptIds === id) {
              flag = true;
            }
          })
          return flag;
        })
      }

      if (param.channelName !== '') {
        responseData.pageData = responseData.pageData.filter(item => {
          return item.channelName.indexOf(param.channelName) !== -1;
        })
      }

      if (param.operateType !== '') {
        responseData.pageData = responseData.pageData.filter(item => {
          return item.operateType === param.operateType;
        })
      }

      if (param.syncFlags !== '') {
        responseData.pageData = responseData.pageData.filter(item => {
          return item.syncFlags === param.syncFlags;
        })
      }


      if (responseData.pageData.length >= pageNum * pageSize) {
        responseData.pageData = responseData.pageData.slice((pageNum - 1) * pageSize, pageNum * pageSize);
      }

      responseData.totalRows = responseData.pageData.length;
      responseData.totalPage = Math.ceil(responseData.totalRows/responseData.pageSize)


      return {
        code: 20000,
        data: responseData,
        errMsg: 'success',
        success: true
      }
    }
  },

  // 查询卡片授权
  {
    url: '/CardSolution/card/accessControl/doorAuthority/bycondition/combined',
    type: 'post',
    response: config => {
      const {pageNum, pageSize, param} = config.body;
      let data = {
        currentPage: pageNum,
        pageSize: pageSize
      }

      data.pageData = acsCardData;

      if (param.cardNumber !== '') {
        data.pageData = data.pageData.filter(item => {
          return item.cardNumber === param.cardNumber;
        })
      }

      /*if (param.personName !== '') {
        data.pageData = data.pageData.filter(item => {
          // 模糊查询用这个
          return item.personName.indexOf(param.personName) !== -1;
        })
      }

      if (param.personCode !== '') {
        data.pageData = data.pageData.filter(item => {
          return item.personCode === param.personCode;
        })
      }

      if (param.authorizeStatus !== '') {
        data.pageData = data.pageData.filter(item => {
          return item.authorizeStatus === param.authorizeStatus;
        })
      }

      if (param.taskStatus !== '') {
        data.pageData = data.pageData.filter(item => {
          return item.taskStatus = param.taskStatus;
        })
      }

      if (data.pageData.length >= pageNum * pageSize) {
        data.pageData = data.pageData.slice((pageNum - 1) * pageSize, pageNum * pageSize);
      }*/

      data.totalpage = Math.ceil(data.pageData.length / pageSize);
      data.totalrow = data.pageData.length

      return {
        code:20000,
        data: data,
        errMsg: 'success',
        success: true
      }
    }
  }
]
