const Mock = require('mockjs')

const cardGroup = [
  {
    groupName: "分组1",
    groupType: 1,
    id: 1,
    memo: "默认分组1"
  },
  {
    groupName: "分组2",
    groupType: 1,
    id: 2,
    memo: "默认分组2"
  },
  {
    groupName: "分组3",
    groupType: 1,
    id: 3,
    memo: "默认分组3"
  },
  {
    groupName: "分组4",
    groupType: 1,
    id: 4,
    memo: "默认分组4"
  },
  {
    groupName: "分组5",
    groupType: 1,
    id: 5,
    memo: "默认分组5"
  },
  {
    groupName: "分组6",
    groupType: 1,
    id: 6,
    memo: "默认分组6"
  },
]

// 授权人脸列表
const person = [
  {
    certificateType: "20",
    faceAccredit: 1,
    faceEndTime: "2036-01-01",
    faceStartTime: "2019-03-20",
    id: 11,
    identityCard: "31231321",
    isValid: 0,
    owner: "test1",
    ownerCode: "12312",
    ownerSex: 1,
    roomNumber: "01001011"
  },
  {
    certificateType: "20",
    faceAccredit: 1,
    faceEndTime: "2036-01-01",
    faceStartTime: "2019-03-20",
    id: 11,
    identityCard: "31231321",
    isValid: 0,
    owner: "test2",
    ownerCode: "12312",
    ownerSex: 1,
    roomNumber: "01001011"
  },
  {
    certificateType: "20",
    faceAccredit: 1,
    faceEndTime: "2036-01-01",
    faceStartTime: "2019-03-20",
    id: 11,
    identityCard: "31231321",
    isValid: 0,
    owner: "test3",
    ownerCode: "12312",
    ownerSex: 1,
    roomNumber: "01001011"
  },
  {
    certificateType: "20",
    faceAccredit: 1,
    faceEndTime: "2036-01-01",
    faceStartTime: "2019-03-20",
    id: 11,
    identityCard: "31231321",
    isValid: 0,
    owner: "test4",
    ownerCode: "12312",
    ownerSex: 1,
    roomNumber: "01001011"
  },
  {
    certificateType: "20",
    faceAccredit: 1,
    faceEndTime: "2036-01-01",
    faceStartTime: "2019-03-20",
    id: 11,
    identityCard: "31231321",
    isValid: 0,
    owner: "test5",
    ownerCode: "12312",
    ownerSex: 1,
    roomNumber: "01001011"
  },
  {
    certificateType: "20",
    faceAccredit: 1,
    faceEndTime: "2036-01-01",
    faceStartTime: "2019-03-20",
    id: 11,
    identityCard: "31231321",
    isValid: 0,
    owner: "test6",
    ownerCode: "12312",
    ownerSex: 1,
    roomNumber: "01001011"
  }

]

// 分组关联设备
const combinations = [
  {
    id: '', // 分组id
    code: ''// 人员id
  }
]

// 人脸授权任务
const faceAuthTasks = [
  {
    channelSeq: 0,
    deviceCode: "1000006",
    deviceId: 1,
    deviceName: "园区围墙机_01",
    draw: 0,
    errorMsgs: "无",
    id: 2,
    operTim: "2019-03-20",
    operType: 1,
    operTypeStr: "增加",
    ownerName: "1访客",
    pageNum: 1,
    pageSize: 20,
    roomNumber: "01001011",
    stat: 5,
    statStr: "设备离线",
    unitSeq: 0,
    validEndTime: "2036-01-01",
    validStartTime: "2019-03-20"
  },
]

module.exports = [
  {
    url: '/vims/face/page/accredit',
    type: 'get',
    response: config => {
      const items = person
      return {
        code: 20000,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  },
  {
    url: '/vims/cardGroup/list',
    type: 'post',
    response: config => {
      return {
        code: 20000,
        data: {
          data: cardGroup
        }
      }
    }
  },
  {
    url: '/vims/face/auth/api',
    type: 'post',
    response: config => {
      let addData = config.body
      console.log(addData)
      let groupIds = addData.groupIds
      let owners = addData.owners
      owners.forEach((item) => {
        let owner = {
          certificateType: "20",
          faceAccredit: 1,
          faceEndTime: item.faceEndTime,
          faceStartTime: item.faceStartTime,
          id: 11,
          identityCard: "31231321",
          isValid: 0,
          owner: "test" + item.ownerCode,
          ownerCode: item.ownerCode,
          ownerSex: 1,
          roomNumber: "01001011"
        }
        person.unshift(owner)
      })
      // 是整体保存，还是一个一个保存
      groupIds.forEach((group) => {
        owners.forEach((owner) => {
          let combination = {
            id: group,
            code: owner
          }
          combinations.push(combination)
        })
      })

      return {
        code: 20000,
        data: {
          errMsg: "success",
          success: true
        }
      }
    }
  },
  {
    url: '/vims/faceTask/list',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          pageData: faceAuthTasks
        }
      }
    }
  },
  {
    url: '/vims/faceTask/list/add',
    type: 'post',
    response: config => {
      let task = {
        channelSeq: 0,
        deviceCode: faceAuthTasks[0].deviceCode + 1,
        deviceId: faceAuthTasks[0].deviceId + 1,
        deviceName: "园区围墙机_01",
        draw: 0,
        errorMsgs: "无",
        id: 2,
        operTim: "2019-03-20",
        operType: 1,
        operTypeStr: "增加",
        ownerName: "1访客",
        pageNum: 1,
        pageSize: 20,
        roomNumber: "01001011",
        stat: 5,
        statStr: "设备离线",
        unitSeq: 0,
        validEndTime: "2036-01-01",
        validStartTime: "2019-03-20"
      }
      faceAuthTasks.unshift(task)
      return {
        code: 20000,
        data: 'success'
      }
    }
  },
]
