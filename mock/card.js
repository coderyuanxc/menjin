const Mock = require('mockjs')
const data1 = Mock.mock({
  'items|30': [{
    id: '@id',
    name: '@sentence(2, 10)',
    'type|1': ['published', 'draft', 'deleted'],
    parentId: '@integer(1, 20)',
    terminalId: '@integer(300, 1500)',
    description: 'mock data'
    //pageviews: '@integer(300, 5000)'
  }]
})

const data3 = [
      {
        "availableTimes": 0,
        "cardCash": 0,
        "cardCost": 0,
        "cardDeposit": 0,
        "cardNumber": "64D6CA71",
        "cardPassword": "123456",
        "cardStatus": "激活",
        "cardSubsidy": 0,
        "cardType": "普通卡",
        "category": "0",
        "deptId": 40,
        "deptName": "黑",
        "endDate": 1868544000000,
        "id": 3,
        "isMainCard": 1,
        "personCode": "6996",
        "personId": 6,
        "personName": "黑1",
        "startDate": 1552924800000,
        "strEndDate": "2029-03-19",
        "strStartDate": "2019-03-19",
        "subSystems": "1,3,4,5,6,7"
      },
      {
        "availableTimes": 255,
        "cardCash": 0,
        "cardCost": 0,
        "cardDeposit": 0,
        "cardNumber": "00000015",
        "cardPassword": "123456",
        "cardStatus": "激活",
        "cardSubsidy": 0,
        "cardType": "普通卡",
        "category": "0",
        "deptId": 41,
        "deptName": "导入导出测试部门",
        "endDate": 1868630400000,
        "id": 5,
        "isMainCard": 1,
        "personCode": "11001",
        "personId": 7,
        "personName": "导入1",
        "startDate": 1553011200000,
        "strEndDate": "2029-03-20",
        "strStartDate": "2019-03-20",
        "subSystems": "1,5,6"
      },
  {
    "availableTimes": 0,
    "cardCash": 0,
    "cardCost": 0,
    "cardDeposit": 0,
    "cardNumber": "B2345678",
    "cardPassword": "123456",
    "cardStatus": "空白",
    "cardSubsidy": 0,
    "cardType": "普通卡",
    "category": "0",
    "deptId": 0,
    "deptName": "",
    "endDate": 1905523200000,
    "id": 2,
    "isMainCard": 0,
    "personCode": "",
    "personId": 0,
    "personName": "",
    "serialNumber": "0002",
    "startDate": 1589990400000,
    "strEndDate": "2030-05-21",
    "strStartDate": "2020-05-21",
    "subSystems": "1,3,4,5,6,7"
  }
    ]





const data2 = Mock.mock({
  'items|30': [{
    id: '@increment',
    name: '@sentence(2, 10)',
    'type|1': ['published', 'draft', 'deleted'],
    createBy: '@integer(1, 20)',
    default: 'false'
    //pageviews: '@integer(300, 5000)'
  }]
})

module.exports = [
  {
    url: '/vue-admin-template/card/list',
    type: 'get',
    response: config => {
      const { cardNumber, personCode, cardStatus,pageSize = 20,  pageNum = 1 } = config.query
      let mockList = data3.filter(item => {
        if (cardNumber && item.cardNumber.indexOf(cardNumber) < 0) return false
        if (cardStatus && item.cardStatus.indexOf(cardStatus) < 0) return false
        if (personCode && item.personCode.indexOf(personCode) < 0) return false

        return true
      })

      const pageList = mockList.filter((item, index) => index < pageSize * pageNum && index >= pageSize * (pageNum - 1))
      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/vue-element-admin/article/list',
    type: 'get',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/vue-element-template/card/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  },
  {
    url: '/vue-admin-template/consume/timeList',
      type: 'get',
    response: config => {
    const items = data2.items
    return {
      code: 20000,
      data: {
        total: items.length,
        items: items
      }
    }
  }
  },
]
