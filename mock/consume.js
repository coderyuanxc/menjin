const Mock = require('mockjs')

const data1 = Mock.mock({
  'items|30': [{
    id: '@id',
    name: '@sentence(2, 10)',
    'type|1': ['published', 'draft', 'deleted'],
    parentId: '@integer(1, 20)',
    terminalId: '@integer(300, 1500)',
    description: 'mock data'
    //pageviews: '@integer(300, 5000)'
  }]
})

const data3 = [
  {
    id: 1,
    name: '陈三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 2,
    name: '李三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 3,
    name: '吴三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 4,
    name: '高三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 5,
    name: '周三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 6,
    name: '奥三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 7,
    name: '李三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 8,
    name: '林三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 9,
    name: '张三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  },
  {
    id: 10,
    name: '叶三',
    type: 'deleted',
    parentId: 123,
    terminalId: 12345,
    description: '速度想念速写冥思苦想迷彩。'
  }
  ]



const data2 = Mock.mock({
  'items|30': [{
    id: '@increment',
    name: '@sentence(2, 10)',
    'type|1': ['published', 'draft', 'deleted'],
    createBy: '@integer(1, 20)',
    default: 'false'
    //pageviews: '@integer(300, 5000)'
  }]
})

module.exports = [
  {
    url: '/vue-admin-template/consume/list',
    type: 'get',
    response: config => {
      const items = data3
      return {
        code: 20000,
        data: {
          total: data3.length,
          items: items
        }
      }
    }
  },
  {
    url: '/vue-element-template/consume/create',
    type: 'post',
    response: config => {
      let temp = config.body
      temp.id = data3.length + 1;
      temp.type = 'draft'
      temp.terminalId = '12344'
      data3.unshift(temp)
      return {
        code: 20000,
        data: 'success'
      }
    }
  },
  {
    url: '/vue-element-template/consume/delete',
    type: 'delete',
    response: config => {
      let temp = config.body
      data3.splice(temp,1)
      return {
        code: 20000,
        data: 'success'
      }
    }
  },
  {
    url: '/vue-admin-template/consume/timeList',
      type: 'get',
    response: config => {
    const items = data2.items
    return {
      code: 20000,
      data: {
        total: items.length,
        items: items
      }
    }
  }
  },
]
