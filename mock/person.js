const Mock = require('mockjs')

const person = [
  {
    code: "10007101",
    name: "test1",
  },
  {
    code: "10007102",
    name: "test2",
  },
  {
    code: "10007103",
    name: "test3",
  },
  {
    code: "10007104",
    name: "test4",
  },
  {
    code: "10007105",
    name: "test5",
  },
  {
    code: "10007106",
    name: "test6",
  },
  {
    code: "10007107",
    name: "test7",
  },
  {
    code: "10007108",
    name: "test8",
  },
  {
    code: "10007109",
    name: "test9",
  },
  {
    code: "100071010",
    name: "test10",
  }

]

module.exports = [
  {
    url: '/CardSolution/card/person/bycondition/combined',
    type: 'post',
    response: config => {
      let query = config.body
      return {
        code: 20000,
        data: {
          total: person.length,
          pageData: person
        }
      }
    }
  },
  {
    url: '/vue-element-template/table/create',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]
