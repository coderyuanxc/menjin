const Mock = require('mockjs')

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(
    Mock.mock({
      cardNumber: '@string("number", 10)',
      deptId: '@string("number", 3)',
      personCode: '@string("number", 6)',
      personId: '@string("number", 6)',
      personName: '@cname',
      reportDate: '@date("yyyy-MM-dd")',
      rechargeAmount: '@float(0, 1000, 0, 2)',
      refundAmount: '@float(0, 1000, 0, 2)',
      subsidyAmount: '@float(0, 1000, 0, 2)'
    })
  )
}

function dateCompare(date1,date2){
  date1 = date1.replace(/\-/gi,"/");
  date2 = date2.replace(/\-/gi,"/");
  var time1 = new Date(date1).getTime();
  var time2 = new Date(date2).getTime();
  console.log(time1, time2)
  if(time1 > time2){
    return false;
  }else{
    return true;
  }
}

module.exports = [
  {
    url: '/vue-admin-template/table/cash',
    type: 'get',
    response: config => {
      const { pageNum, pageSize, startDate, endDate, deptIdsString, personName, personCode } = config.query
      // console.log(startDate)

      const mockList = List.filter(item => {
        if (deptIdsString && item.deptId !== deptIdsString) return false
        if (personName && item.personName.indexOf(personName) < 0) return false
        if (personCode && item.personCode !== personCode) return false
        // console.log(startDate)  // 2021-4-11
        if (startDate && dateCompare(item.reportDate, startDate)) return false
        if (startDate && dateCompare(endDate, item.reportDate)) return false
        // console.log(startDate, item.reportDate, endDate)  // 打印2003-1-1 2006-10-14 2017-2-24
        return true
      })

      const pageData = mockList.filter((item, index) => index < pageSize * pageNum && index >= pageSize * (pageNum - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageData
        }
      }
    }
  }
]
